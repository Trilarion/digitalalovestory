init:
    image blue = Solid((0,85,170,215))
    image black = Solid((0,0,0,255))
    image boot = "gfx/bootscreen.png"

init python:
    import time, random
    if not persistent.volume_init:
        _preferences.set_volume("music", 0.5)
        persistent.volume_init = True
    
    _game_menu_screen = None
    messages = [ ]
    bbs_messages = { "library":[ ] }
    queued_messages = { "library":[ ] }
    open_bbs = None
    read_messages = [ ]
    replied_to = [ ]
    downloaded = [ ]
    current_label = ("desktop", "", "")
    selected = None
    real_name = ""
    screen_name = ""
    system_message_text = ""
    notepad_screen = "Phone #s"
    notepad = { "Phone #s":[ ], "c0dez":[ ], "Passwords":[ ] }
    dead_c0dez = [ ]
    code_message_counter = 1
    logged_in = False
    drag_positions = { }
    first_run = True
    vram_overflow = False
    version = "1.3"
    music_playing = 1.0
    
    magic_posts = [ ]
    flags = [ ]
    
    persist = MultiPersistent("scoutshonour.com")
    
    BLACK_SCREEN = Animation("welcome/crash1.png", 1.5, "welcome/crash2.png", 0.5, "welcome/crash3.png", 0.75, "welcome/crash4.png", 2.0)
    
    CALLINGCARD = "915-3347"
    LIBRARY = "(212) 561-2910"
    LAKECITY = "698-5519"
    MATRIX = "220-7683"
    SECTOR001 = "(614) 622-1701"
    GIBSON = "(714) 402-5691"
    ARPANET = "(805) 524-4742"
    
    BBS_NAMES = { LIBRARY:"The Underground Library", LAKECITY:"Lake City Local", MATRIX:"The Matrix", SECTOR001:"Sector 001", GIBSON:"GibsonBBS", ARPANET:"ARPANET" }
    
    STARS_COME_OUT = "music/5 the stars come out.mp3"
    SPACE_BEACON = "music/radiantx-space_beacon.mp3"
    ITS_DARK = "music/it's dark, raining and the leaves certainly aren't done falling.mp3"
    PAPER_DOLLS = "music/4mat-Paper_Dolls.ogg"
    WAKE_UP = "music/Sanczo Zapiekanka - Wake Up.ogg"
    DISAPPOINTMENT = "music/dissapointment.mp3"
    CANNOT_LOVE = "music/4mat-(Day06)-One_Cannot.ogg"
    FUTURE = "music/05 Future, and It Doesn't Work.ogg"
    
    SONGS = { STARS_COME_OUT:("Stars Come Out", "Brother Android"), SPACE_BEACON:("Space Beacon", "radiantx"), ITS_DARK:("It's dark, raining...", "ninetales"), PAPER_DOLLS:("Paper Dolls", "4mat"), WAKE_UP:("Wake Up", "Sanczo Zapienkaka"), DISAPPOINTMENT:("Disappointment", "8-Beat"), CANNOT_LOVE:("One Cannot Love and Do Nothing", "4mat") }
    
    AI_LIST = [ "Emilia", "Blue Sky", "Delphi", "Montjoy", "Paris" ]
    
    NOUNS = ['eighths', 'entry', 'hygiene', 'spoke', 'lifetimes', 'mediums', 'initiators', 'catches', 'confinement', 'counselors', 'cams', 'judgment', 'oceans', 'outline', 'cushions', 'limb', 'shift', 'chamber', 'workbooks', 'oxygen', 'roll', 'habit', 'litre', 'prisoner', 'vibration', 'debris', 'maximum', 'sockets', 'photographs', 'model', 'catch', 'note', 'jumper', 'cells', 'retrievals', 'judge', 'dependence', 'sharpener', 'brushes', 'individuals', 'perfect', 'cellar', 'justice', 'area', 'pits', 'appeals', 'stripes', 'vouchers', 'propose', 'launcher', 'incentive', 'observations', 'solution', 'infections', 'fibers', 'defense', 'shadows', 'pots', 'prepositions', 'tomorrow', 'platforms', 'berries', 'clerks', 'tie', 'decrements', 'kill', 'operator', 'blankets', 'performance', 'hoops', 'shot', 'dittos', 'moves', 'nail', 'dollars', 'circles', 'responses', 'brightness', 'berthings', 'outfit', 'blade', 'district', 'cartridge', 'procedures', 'berry', 'electronics', 'breast', 'hickory', 'hit', 'ounce', 'stools', 'library', 'designators', 'day', 'hub', 'builders', 'manufacturer', 'deputy', 'console', 'lung', 'directives', 'bills', 'helms', 'lists', 'spool', 'disk', 'overcoats', 'atmospheres', 'boilers', 'threats', 'cars', 'thickness', 'dent', 'massed', 'directories', 'swallows', 'ventilation', 'lifetime', 'operability', 'pokes', 'bilge', 'user', 'circulations', 'erasers', 'sterilizer', 'lots', 'expenditure', 'dears', 'adjustments', 'dew', 'readers', 'auditor', 'rockets', 'change', 'rooms', 'presences', 'forks', 'components', 'interval', 'protests', 'truths', 'schedules', 'tachometers', 'wedding', 'divers', 'citizens', 'desire', 'writing', 'bore', 'recapitulation', 'alternate', 'month', 'buckles', 'wastes', 'limit', 'origins', 'stacks', 'twirl', 'rust', 'welder', 'victims', 'solvent', 'varieties', 'cage', 'capes', 'rounds', 'photograph', 'allotments', 'lifeboats', 'circumferences', 'transmittals', 'yards', 'subdivision', 'purchases', 'duration', 'friction', 'pounds', 'text', 'nylon', 'saddle', 'thermometer', 'petroleum', 'hug', 'keyboard', 'continuity', 'greases', 'flares', 'vines', 'powers', 'kettle', 'permits', 'method', 'developments', 'listings', 'way', 'maples', 'hunk', 'rainbows', 'pieces', 'jellies', 'escape', 'slope', 'umbrellas', 'sunshine', 'percents', 'displacement', 'compounds', 'attraction', 'update', 'wonder', 'glasses', 'rose', 'transmissions', 'interruptions', 'paragraph', 'stomach', 'letter', 'bags', 'worlds', 'tower', 'hall', 'backup', 'hilltops', 'ignitions', 'stages', 'priority', 'warranty', 'paygrades', 'bow', 'police', 'stakes', 'worm', 'rib', 'shield', 'fare', 'base', 'partition', 'logs', 'mouths', 'azimuth', 'edge', 'ratings', 'evacuation', 'gland', 'trash', 'scab', 'curls', 'splice', 'sabotage', 'hierarchies', 'jig', 'lashes', 'experience', 'education', 'voids', 'nonavailabilities', 'fog', 'pistol', 'detonations', 'attempts', 'dart', 'intents', 'prism', 'binders', 'brakes', 'rifle', 'disadvantage', 'confinements', 'dereliction', 'secretaries', 'claims', 'sod', 'mode', 'concern', 'aprons', 'journal', 'ropes', 'semiconductor', 'televisions', 'macros', 'stitches', 'hail', 'cables', 'gasoline', 'materials', 'chart', 'quantity', 'leap', 'coordinate', 'suspects', 'fires', 'sleep', 'hitches', 'fridays', 'arrows', 'disciplines', 'glide', 'executive', 'electrician', 'anthem', 'translators', 'semicolons', 'tastes', 'superstructures', 'claw', 'party', 'rejection', 'choices', 'impact', 'peck', 'splints', 'smash', 'introductions', 'casualty', 'systems', 'texts', 'chances', 'asterisks', 'vapor', 'turbine', 'charges', 'profession', 'roofs', 'blaze', 'scratch', 'cases', 'auditors', 'rates', 'milestones', 'octobers', 'greenwich', 'masses', 'diaries', 'talks', 'operand', 'falls', 'dip', 'track', 'shelters', 'session', 'conventions', 'paws', 'number', 'section', 'laws', 'lieutenants', 'poke', 'litres', 'workloads', 'relocation', 'starboard', 'crews', 'wines', 'edges', 'alternates', 'storms', 'bather', 'apostrophes', 'team', 'braid', 'grams', 'submarined', 'graphs', 'arrangement', 'deficiencies', 'pressures', 'photo', 'clap', 'openings', 'contraband', 'dams', 'pints', 'rescues', 'stator', 'nerve', 'walks', 'tours', 'income', 'potato', 'commissions', 'industries', 'policy', 'travels', 'dress', 'evenings', 'reservists', 'tail', 'sirens', 'associate', 'semiconductors', 'brooms', 'swells', 'attacker', 'slopes', 'skirt', 'airships', 'dependencies', 'treatments', 'secret', 'torques', 'fits', 'profits', 'steamers', 'surplus', 'schedulers', 'missile', 'classifications', 'cities', 'passage', 'interviewer', 'register', 'exterior', 'blueprints', 'reels', 'lane', 'objective', 'dissipation', 'failures', 'radar', 'arrests', 'injectors', 'generation', 'lift', 'bulkhead', 'stern', 'instructors', 'jack', 'discrimination', 'emitter', 'fighting', 'debit', 'quarterdecks', 'rumbles', 'time', 'briefing', 'rocket', 'signatures', 'azimuths', 'ballast', 'exposures', 'implementations', 'vendors', 'rice', 'millimeter', 'dimensions', 'wheel', 'priorities', 'hotels', 'waxes', 'communities', 'sum', 'atom', 'regulators', 'twists', 'map', 'dishes', 'network', 'gasolines', 'solids', 'heap', 'block', 'fuel', 'west', 'alcoholic', 'giants', 'songs', 'amount', 'weave', 'hair', 'tons', 'couple', 'moment', 'hatchets', 'schools', 'shirts', 'establishment', 'price', 'pavement', 'trips', 'contrast', 'licks', 'skins', 'claps', 'mistrials', 'click', 'capital', 'transistors', 'tanks', 'club', 'carburetors', 'honors', 'increases', 'receivers', 'diesels', 'islands', 'presumption', 'graph', 'laboratory', 'jacks', 'abettor', 'candles', 'highlines', 'pad', 'correspondence', 'sweeps', 'admirals', 'verb', 'watts', 'subroutine', 'basket', 'subordinate', 'round', 'taps', 'diagram', 'oxides', 'thimbles', 'mount', 'mountain', 'catchers', 'duties', 'squares', 'deletion', 'languages', 'book', 'links', 'forecast', 'professionalism', 'forecastles', 'relay', 'kinds', 'conn', 'collections', 'haste', 'range', 'matches', 'steps', 'ramp', 'boot', 'detent', 'guys', 'tents', 'altimeter', 'rescuer', 'partner', 'teaching', 'stone', 'fish', 'shores', 'indications', 'integrity', 'revolution', 'desertions', 'drums', 'signs', 'calibers', 'toothpicks', 'staples', 'strand', 'drunkeness', 'wiggles', 'secrets', 'fumes', 'pilots', 'slides', 'modes', 'consequences', 'paw', 'equivalents', 'presidents', 'minuses', 'collisions', 'chin', 'administrations', 'tractor', 'cannons', 'paper', 'self', 'assembly', 'stream', 'jails', 'safeguards', 'submissions', 'condition', 'zip', 'radars', 'picks', 'jackboxes', 'ampere', 'sky', 'government', 'watch', 'gunnery', 'chapter', 'stomachs', 'circumstances', 'artillery', 'mist', 'help', 'drill', 'desert', 'crowd', 'leaving', 'exits', 'matter', 'place', 'subtasks', 'ride', 'convulsions', 'glove', 'clock', 'face', 'type', 'sources', 'collection', 'shipmates', 'authorization', 'threads', 'minerals', 'bushel', 'twigs', 'love', 'entrances', 'ammunition', 'towel', 'alternatives', 'refund', 'specialist', 'goods', 'reasons', 'elapse', 'data', 'tissue', 'bristle', 'earth', 'tuition', 'slices', 'configurations', 'coxswain', 'puddle', 'laboratories', 'fashion', 'employee', 'lifts', 'marbles', 'bottoms', 'semaphore', 'radiators', 'deck', 'welds', 'sevens', 'peas', 'peg', 'sector', 'consequence', 'originators', 'algorithms', 'helicopters', 'miner', 'respect', 'nickel', 'aluminum', 'filters', 'bureaus', 'inventories', 'squadron', 'bullets', 'assault', 'focuses', 'nets', 'company', 'tries', 'techniques', 'angle', 'accesses']

    
    class BBS():
        def __init__(self, image, password="new"):
            self.welcome = "welcome/" + image
            self.queued = [ ]
            self.stored = [ ]
            self.password = password
            self.down = False
        def store(self, label, sender, attachment=None):
            if attachment:
                message = (label, sender, attachment)
            else:
                message = (label, sender)
            if message not in self.stored:
                self.stored.append(message)
        def queue(self, label, sender, attachment=None):
            if attachment:
                message = (label, sender, attachment)
            else:
                message = (label, sender)
            if message not in messages and message not in self.queued:
                self.queued.append(message)
        def reset(self):
            self.queued = [ ]
            self.stored = [ ]
    bbs = { }
    
    windows_open = { "messages":False, "music":False, "bbs":False, "bbs_messages":False, "bbs_send":False, "notepad":False, "lcl_archives":False }
    downloaded = [ ]
    can_save = False
    
    def block_background():
        ui.tag("fade")
        ui.button(xfill=True, yfill=True, background=Solid((0,0,0,64)), clicked=do_nothing)
        ui.null()
    def refresh():
        renpy.restart_interaction()
        renpy.jump(current_label[0].split("^")[0].lower().replace(" ", "_").replace(".", "_").replace("-", "_").replace(":", "").replace("'", "").replace("(", "").replace(")", "").replace(",", "").replace("!", "").replace("?", "").replace("*", ""))
    def get_messages():
        global messages
        def all_in(a, b):
            for i in a:
                if i not in b:
                    return False
            return True
        for i in magic_posts:
            if len(read_messages) > i[0] and all_in(i[1], flags):
                bbs[LAKECITY].queue(i[2], i[3])
                magic_posts.remove(i)
        if bbs[open_bbs].queued:
            while bbs[open_bbs].queued:
                messages += [ bbs[open_bbs].queued.pop() ]
            system_message("New private messages downloaded!")
    def do_nothing():
                pass
    def set_open(to_set):
        windows_open[to_set] = True
        refresh()
    def make_window(name, xpos=0, ypos=0, close_func=None, **args):
        global drag_positions
        if name not in drag_positions:
            drag_positions[name] = (xpos, ypos)
        #ui.at(Draggable(name))
        ui.window(background=Frame("gfx/frame.png", xborder=40, yborder=30), ypos=ypos, xpos=xpos, xfill=False, **args)
        ui.vbox()
        ui.hbox(xpos=6, ypos=-3)
        ui.imagebutton("gfx/close.png", "gfx/close.png", clicked=close_func)
        ui.window(background=Solid((255,255,255,255)), yminimum=0, xfill=False, xanchor=0, xpos=6, yalign=0, ypadding=0)
        ui.text(name, color="#0055aa")
        ui.close()
    def scroll_bar(vp):
        def scroll(vp, dir):
                vp.yadjustment.change(vp.yadjustment.get_value()+(28*dir))
        ui.imagebutton("gfx/uparrow.png", "gfx/uparrow.png", clicked=renpy.curry(scroll)(vp, -1))
        ui.bar(style="vscrollbar", adjustment=vp.yadjustment)
        ui.imagebutton("gfx/downarrow.png", "gfx/downarrow.png", clicked=renpy.curry(scroll)(vp, 1))
    def loadsave():
        def open(file):
            renpy.load(file[0])
            return True
        def save(file):
            renpy.take_screenshot()
            renpy.save(file[0], save_name)
            return True
        def cmp(a, b):
            return int(a[3])-int(b[3])
        new_file = 0
        avail_saves = renpy.list_saved_games(regexp=r'[0-9].|auto-[0-3]\b')
        avail_saves.sort(cmp, reverse=True)
        for i in renpy.list_saved_games():
            if i not in avail_saves:
                renpy.unlink_save(i[0])
        for i in avail_saves:
            if not i[0].startswith("auto-"):
                if int(i[0]) > int(new_file):
                    new_file = i[0]
        new_file = str(int(new_file)+1)
        ui.layer("system")
        if can_save: offset = 24
        else: offset = 0
        ui.sizer(maxheight=640-24-offset)
        make_window("Save/Load", 0, offset, ui.returns(1), xminimum=1024)
        ui.hbox()
        vp = ui.viewport(ymaximum=640-24-offset, xmaximum=970, mousewheel=True)
        ui.vbox()
        if can_save:
            ui.null(height=3)
            ui.button(clicked=renpy.curry(save)((new_file,)), ypadding=11)
            ui.text("New save file", text_align=0.5, minwidth=969, color="#0055aa", hover_color="#ffffff")
        for i in avail_saves:
            ui.null(height=3)
            ui.hbox()
            if i[0].startswith("auto-") or not can_save:
                ui.button()
            else:
                pass
                ui.button(clicked=renpy.curry(save)(i))
            ui.vbox()
            ui.hbox()
            if i[0].startswith("auto-"):
                ui.text("Auto-save  ", color="#0055aa", hover_color="#ffffff", minwidth=250)
            else:
                ui.text("Save file #" + i[0], color="#0055aa", hover_color="#ffffff", minwidth=250)
            ui.text(i[1], color="#ff9000", hover_color="#ffffff")
            ui.close()
            ui.text(time.ctime(int(i[3])), minwidth=730, color="#0055aa", hover_color="#ffffff")
            ui.close()
            ui.null(width=3)
            ui.button(clicked=renpy.curry(open)(i), ypadding=12)
            ui.text("Load file ", minwidth=210, color="#0055aa", hover_color="#ffffff", text_align=0.5)
            ui.close()
        ui.close()
        ui.vbox(ypos=-1,xpos=20)
        scroll_bar(vp)
        ui.close()
        ui.close() #vbox
        ui.close() #hbox
        ui.close() #window
        if not can_save:
            scanlines_overlay()
        ui.interact()
    def preferences():
        def update_volume(x):
            _preferences.set_volume("music", x)
        def update_sound(x):
            _preferences.set_volume("sfx", x)
        def prompt():
            if yesno("Are you sure you want to quit? All unsaved changes will be lost."):
                renpy.full_restart()
            else:
                renpy.restart_interaction()
                renpy.jump("preferences")
        def fullscreen(value):
            _preferences.fullscreen = value
        def change_sn():
            global screen_name
            screen_name = input("New screen name:")
            renpy.restart_interaction()
            renpy.jump("preferences")
        def change_rn():
            global first_name, last_name
            first_name = input("New first name:")
            last_name = input("New last name:")
            renpy.restart_interaction()
            renpy.jump("preferences")
        def toggle_scanlines():
            if persistent.hide_scanlines:
                persistent.hide_scanlines = False
            else:
                persistent.hide_scanlines = True
            refresh()
        ui.layer("system")
        ui.sizer(maxwidth=400, xalign=0.5)
        ui.window(yalign=0.5, background=None)
        make_window("Control Panel", 0, 24, refresh)
        ui.null(height=10)
        ui.vbox()
        ui.text("Display mode")
        ui.textbutton("Window", clicked=renpy.curry(fullscreen)(False), xfill=True, left_margin=40)
        ui.textbutton("Fullscreen", clicked=renpy.curry(fullscreen)(True), xfill=True, left_margin=40)
        ui.null(height=10)
        ui.textbutton("Toggle scanlines", clicked=toggle_scanlines, xfill=True, left_margin=40)
        ui.null(height=10)
        ui.hbox()
        ui.text("Volume   ")
        v = ui.adjustment(value=_preferences.get_volume("music"), range=1.0, changed=update_volume)
        ui.bar(style="bar", adjustment=v)
        ui.close()
        ui.hbox()
        ui.text("Modem   ")
        ui.null(width=8)
        m = ui.adjustment(value=_preferences.get_volume("sfx"), range=1.0, changed=update_sound)
        ui.bar(style="bar", adjustment=m)
        ui.close()
        ui.null(height=40)
        ui.textbutton("Change screen name", clicked=change_sn, xfill=True)
        ui.textbutton("Change real name", clicked=change_rn, xfill=True)
        ui.null(height=40)
        ui.textbutton("Shutdown Workbench", clicked=prompt, xfill=True)
        ui.close()
        ui.close()
        ui.close()
    def yesno(message):
        ui.layer("system")
        ui.sizer(maxwidth=500, xalign=0.5)
        ui.window(yalign=0.5, background=None)
        make_window("System prompt", 0, 0, None)
        ui.text(message + "\n", text_align=0.5)
        ui.hbox()
        ui.button(clicked=ui.returns(True))
        ui.text("Yes", minwidth=150, text_align=0.5, color="#0055aa", hover_color="#ffffff")
        ui.null(width=125)
        ui.button(clicked=ui.returns(False))
        ui.text("No", minwidth=150, text_align=0.5, color="#0055aa", hover_color="#ffffff")
        ui.close()
        ui.close()
        ui.close()
        return ui.interact()
    def input(prompt, **args):
        ui.layer("system")
        block_background()
        ui.sizer(maxwidth=600, xalign=0.5, yalign=0.5)
        make_window("Query", 0, 200, None)
        ui.text(prompt, minwidth=600)
        ui.window(background=Solid((255,255,255,255)), ymargin=10)
        ui.input("", color="#ff9000", **args)
        ui.close()
        ui.close()
        renpy.scene("scanlines")
        return ui.interact()
    def dialer():
        def dial(to_dial):
            if to_dial in bbs:
                global open_bbs, logged_in
                renpy.music.play("dial.wav", channel="sound")
                renpy.pause(10.0)
                renpy.music.stop(channel="sound")
                bbs_theme(to_dial)
                open_bbs = to_dial
                logged_in = False
                windows_open["bbs_messages"] = False
                windows_open["bbs_send"] = False
                set_open("bbs")
            else:
                renpy.music.play("dialdead.wav", channel="sound")
                renpy.pause(4.0)
                renpy.music.stop(channel="sound")
                system_message("Unable to connect. Are you sure you dialed a listening modem?")
        def dial_input(long_distance=False):
            def format(value):
                if (len(number.content) == 3 and not long_distance) or (len(number.content) == 9 and long_distance):
                    number.update_text(number.content + "-", True)
                if long_distance:
                    if len(number.content) == 3:
                        number.update_text("(" + number.content + ") ", True)
            def backspace():
                if (len(number.content) == 4 and not long_distance) or (len(number.content) == 10 and long_distance):
                    number.update_text(number.content[:-2], True)
                elif long_distance and len(number.content) == 6:
                    number.update_text(number.content.replace("(", "").replace(")", "")[:-2], True)
                else:
                    number.update_text(number.content[:-1], True)
            if windows_open["notepad"]:
                global notepad_screen
                notepad_screen = "Phone #s"
            ui.layer("system")
            block_background()
            ui.sizer(maxwidth=600, xalign=0.5, yalign=0.5)
            make_window("Query", 0, 200, refresh)
            if long_distance:
                ui.text("Please enter the number, including area code, you wish to dial:", minwidth=600)
            else:
                ui.text("Please enter the local number you wish to dial:", minwidth=600)
            ui.hbox(xalign=0.5)
            ui.sizer(maxwidth=300, maxheight=50)
            ui.window(background=Solid((255,255,255,255)), ymargin=10)
            if long_distance: num_length = 14
            else: num_length = 8
            number = ui.input("", color="#ff9000", changed=format, length=num_length, allow="1234567890")
            ui.keymap(K_RETURN=ui.returns(True), K_BACKSPACE=backspace)
            ui.textbutton("Dial", clicked=ui.returns(True), xpos=6, ypos=9, ypadding=6, xpadding=80)
            ui.close()
            ui.close()
            ui.close()
            ui.interact()
            ui.layer("system")
            make_window("Alert", 0, 0, None, xalign=0.5, yalign=0.5)
            ui.text("Now dialing...")
            ui.close()
            ui.close()
            if number.content.rfind(CALLINGCARD) != -1:
                renpy.music.play("dial.wav", channel="sound")
                renpy.pause(6.0)
                renpy.music.stop(channel="sound")
                if windows_open["notepad"]:
                    global notepad_screen
                    notepad_screen = "c0dez"
                if "blackout" in flags:
                    code = input("Due to technical difficulties, our billing system is currently in manual mode, and as such, are unable to process recent changes to your billing information. We apologize for the inconvenience.\n\nPlease enter your calling card access number:")
                else:
                    code = input("Please enter your calling card access number:")
                if code in notepad["c0dez"]:
                    if code not in dead_c0dez and random.random() < 1.0/3:
                        dead_c0dez.append(code)
                        if len(dead_c0dez) == len(notepad["c0dez"]):
                            global code_message_counter
                            code_message_counter += 1
                            bbs[MATRIX].store("New c0dez^" + str(code_message_counter), "RobFugitive")
                    if code in dead_c0dez and "blackout" not in flags:
                        system_message("The access number you have given has been rescinded. Please contact Sprint billing if you believe this to be in error.")
                    else:
                        dial_input(True)
                else:
                    system_message("Sorry, the access number you have given is invalid. Please hang up and try your call again.")
            else:
                dial(number.content)
        dial_input()
    def disconnect():
        global logged_in, open_bbs
        windows_open["bbs_messages"] = False
        windows_open["bbs_send"] = False
        windows_open["lcl_archives"] = False
        windows_open["bbs"] = False
        
        if open_bbs == ARPANET and "emilia_source" in downloaded:
            bbs[ARPANET].down = True
        
        open_bbs = None
        if logged_in:
            system_message("You are now disconnected.")
            logged_in = False
        else:
            refresh()
    def home():
        get_messages()
        windows_open["bbs_messages"] = False
        windows_open["bbs_send"] = False
        windows_open["lcl_archives"] = False
        windows_open["bbs"] = True
        refresh()
    def open_messages():
        get_messages()
        windows_open["bbs"] = False
        windows_open["bbs_send"] = False
        windows_open["lcl_archives"] = False
        windows_open["bbs_messages"] = True
        refresh()
    def open_send():
        get_messages()
        windows_open["bbs"] = False
        windows_open["bbs_messages"] = False
        windows_open["lcl_archives"] = False
        windows_open["bbs_send"] = True
        refresh()
    def lcl_archives():
        windows_open["bbs"] = False
        windows_open["bbs_messages"] = False
        windows_open["bbs_send"] = False
        windows_open["lcl_archives"] = True
        refresh()
    def bbs_menu(current_screen):
        ui.hbox(xalign=0.5)
        if open_bbs == ARPANET:
            if current_screen is not "home":
                ui.textbutton("Home", clicked=home, xpadding=50)
            if current_screen is not "send":
                ui.textbutton("Send EMail", clicked=open_send, xpadding=50)
        elif open_bbs == MATRIX and "lcl_archives" in flags:
            if current_screen is not "lcl":
                ui.textbutton("LCL Archives", clicked=lcl_archives)
            if current_screen is not "home":
                ui.textbutton("Home", clicked=home, xpadding=50)
            if current_screen is not "messages":
                ui.textbutton("Messages", clicked=open_messages)
            if current_screen is not "send":
                ui.textbutton("Send PM", clicked=open_send, xpadding=20)
        else:
            if current_screen is not "messages":
                ui.textbutton("Messages", clicked=open_messages)
            if current_screen is not "home":
                ui.textbutton("Home", clicked=home, xpadding=50)
            if current_screen is not "send":
                ui.textbutton("Send PM", clicked=open_send, xpadding=20)
        ui.textbutton("Disconnect", clicked=disconnect)
        ui.close()
    def scanlines_overlay():
        if not persistent.hide_scanlines:
            ui.layer("scanlines")
            ui.tag("scanlines")
            ui.image("gfx/scanlines.png")
            ui.close()
    def notepad_overlay():
        if windows_open["notepad"]:
            def hide_this():
                windows_open["notepad"] = False
                refresh()
            def change_page(new_page):
                global notepad_screen
                notepad_screen = new_page
                refresh()
            ui.sizer(maxwidth=560, maxheight=250)
            make_window("Notepad", 0, 360, hide_this)
            ui.hbox()
            ui.vbox()
            ui.sizer(maxwidth=517)
            ui.window(background=Solid((255,255,255,255)), top_margin=2)
            vp = ui.viewport(ymaximum=255, xmaximum=517, mousewheel=True)
            ui.vbox()
            for i in notepad[notepad_screen]:
                if notepad_screen == "c0dez" and i in dead_c0dez:
                    ui.text("{s}" + i + "{/s}", color="#000000")
                else:
                    ui.text(i, color="#000000")
            ui.close()
            ui.null(height=3)
            ui.hbox(xalign=0.5)
            for i in [ "Phone #s", "c0dez", "Passwords" ]:
                if i == notepad_screen:
                    ui.textbutton(i, xmargin=3)
                else:
                    ui.textbutton(i, clicked=renpy.curry(change_page)(i), xmargin=3)
            ui.close()
            ui.close()
            ui.vbox(ypos=-1,xpos=6)
            scroll_bar(vp)
            ui.close()
            ui.close()
            ui.close()
    def desktop_overlay():
        if "game_over" not in flags:
            ui.keymap(K_ESCAPE=refresh)
            ui.layer("desktop")
            ui.window(yalign=0, yminimum=0, ypadding=1, background=Solid((255,255,255,255)))
            ui.text("Amie Workbench.  Version " + version + ".  42k free.", color="#0055aa")
            ui.button(clicked=renpy.curry(set_open)("messages"), xanchor=0.5, xpos=0.68, yalign=1.0, background=None, hover_background=Solid((255,144,0,255)))
            ui.vbox()
            ui.image("gfx/mail_icon.png", xalign=0.5)
            ui.text("Messages")
            ui.close()
            if "dialer" in downloaded:
                ui.button(clicked=dialer, xanchor=0.5, xpos=0.9, ypos=1.0, yanchor=1.0, background=None, hover_background=Solid((255,144,0,255)))
                ui.vbox()
                ui.image("gfx/dial_icon.png", xalign=0.5)
                ui.text("Dialer")
                ui.close()
            if "notepad" in downloaded:
                ui.button(clicked=renpy.curry(set_open)("notepad"), xanchor=0.5, xpos=0.1, yalign=1.0, background=None, hover_background=Solid((255,144,0,255)))
                ui.vbox()
                ui.image("gfx/notepad_icon.png", xalign=0.5)
                ui.text("Notepad")
                ui.close()
            ui.button(clicked=renpy.curry(set_open)("music"), xanchor=0.5, xpos=0.9, ypos=32, background=None, hover_background=Solid((255,144,0,255)))
            ui.vbox()
            ui.image("gfx/music_icon.png", xalign=0.5)
            ui.text("Music")
            ui.close()
            if "dict_hacker" in downloaded:
                ui.button(clicked=dictionary_hacker, xanchor=0.5, xpos=0.3, ypos=1.0, yanchor=1.0, background=None, hover_background=Solid((255,144,0,255)))
                ui.vbox()
                ui.image("gfx/padlock_icon.png", xalign=0.5)
                ui.text("dict hacker")
                ui.close()
            if "emilia_source" in downloaded:
                ui.button(clicked=build, xanchor=0.5, xpos=0.9, ypos=0.85, yanchor=1.0, background=None, hover_background=Solid((255,144,0,255)))
                ui.vbox()
                ui.image("gfx/compile_icon.png", xalign=0.5)
                ui.text("build.bat")
                ui.close()
            if "payload_rebuild" in downloaded:
                ui.button(clicked=build_payload, xanchor=0.5, xpos=0.68, ypos=0.85, yanchor=1.0, background=None, hover_background=Solid((255,144,0,255)))
                ui.vbox()
                ui.image("gfx/compile_icon.png", xalign=0.5)
                ui.text("payload.bat")
                ui.close()
            ui.imagebutton("gfx/save.png", "gfx/save.png", clicked=ui.jumps("save"), xalign=1.0, yalign=0, right_padding=3*16)
            ui.imagebutton("gfx/pinwheel.png", "gfx/pinwheel.png", clicked=ui.jumps("preferences"), xalign=1.0, yalign=0, right_padding=3*3)
            ui.close()
    def music_overlay():
        if windows_open["music"]:
            def hide_this():
                windows_open["music"] = False
                refresh()
            current_song = renpy.music.get_playing()
            make_window("Now playing...", 1012, 30, hide_this, xanchor=1.0)
            if current_song:
                global music_playing
                def change(play):
                    global music_playing
                    renpy.music.set_volume(play, channel="music")
                    music_playing = play
                    refresh()
                ui.hbox()
                if music_playing:
                    ui.imagebutton("gfx/pause.png", "gfx/hover_pause.png", clicked=renpy.curry(change)(0), top_padding=2, right_padding=3)
                else:
                    ui.imagebutton("gfx/play.png", "gfx/hover_play.png", clicked=renpy.curry(change)(1.0), top_padding=2, right_padding=3)
                ui.vbox(ypos=3)
                ui.text(SONGS[current_song][0], minwidth=400, text_align=0.5)
                ui.text(SONGS[current_song][1], minwidth=400, text_align=0.5, xfill=True)
                ui.close()
                ui.close()
            else:
                ui.window(ypadding=13, xfill=False)
                ui.text("ERR: Music disabled, check settings")
            ui.close()
                
    def open_message(message):
        global current_label, read_messages
        if message not in read_messages:
            read_messages += [ message ]
        current_label = message
        refresh()
    def show_messages(bbs_name, menu_name="messages"):
        def download_message(message):
            if bbs_name == LIBRARY and version != "1.4.1":
                harass()
                return
            global messages
            messages += [ message ]
            system_message("Message downloaded!")
        ui.sizer(maxheight=490, maxwidth=897, xalign=0.5)
        make_window("BBS Dialer", 0, 24, disconnect)
        ui.hbox()
        vp = ui.viewport(ymaximum=490, xmaximum=859, mousewheel=True)
        ui.vbox()
        for i in bbs[bbs_name].stored:
            ui.hbox()
            ui.text(i[0].split("^")[0].strip("1234567890"), minwidth=450, ypos=3)
            if i[1] in AI_LIST:
                ui.text("*" + i[1], minwidth=200, ypos=3)
            else:
                ui.text(i[1], minwidth=200, ypos=3)
            if i not in read_messages:
                ui.text("New", minwidth=70, ypos=3, color="#ff9000")
            else:
                ui.null(width=70)
            ui.button(clicked=renpy.curry(open_message)(i), background=Solid((255,255,255,255)), hover_background=Solid((255,144,0,255)), ymargin=2)
            ui.text("Open", color="#0055aa", hover_color="#ffffff")
            if i not in messages:
                ui.imagebutton("gfx/download.png", "gfx/hover_download.png", ymargin=2, left_margin=6, clicked=renpy.curry(download_message)(i))
            ui.close()
        ui.close()
        ui.vbox(ypos=-1,xpos=15)
        scroll_bar(vp)
        ui.close()
        ui.close()
        ui.null(height=13, width=897)
        bbs_menu(menu_name)
        ui.close()
    def message_list_overlay():
        if windows_open["bbs_messages"]:
            global windows_open, open_bbs
            show_messages(open_bbs)
        if windows_open["bbs_send"]:
            global windows_open
            users = [ ]
            for i in bbs[open_bbs].stored:
                users.append(i[1])
            ui.sizer(maxheight=490, maxwidth=897, xalign=0.5)
            make_window("BBS Dialer", 0, 24, disconnect)
            ui.hbox()
            vp = ui.viewport(ymaximum=490, xmaximum=859, mousewheel=True)
            ui.vbox()
            for i in set(users):
                ui.hbox()
                if i in AI_LIST:
                    ui.text("*" + i, minwidth=590, ypos=3)
                else:
                    ui.text(i, minwidth=590, ypos=3)
                ui.textbutton("Send message", ypos=3, clicked=renpy.curry(send_message)(i))
                ui.close()
            ui.close()
            ui.vbox(ypos=-1,xpos=15)
            scroll_bar(vp)
            ui.close()
            ui.close()
            ui.null(height=13, width=897)
            ui.hbox(xalign=0.5)
            bbs_menu("send")
            ui.close()
            ui.close()
        if windows_open["lcl_archives"]:
            show_messages(LAKECITY, "lcl")
        if windows_open["messages"]:
            def hide_this():
                global windows_open
                windows_open["messages"] = False
                refresh()
            ui.sizer(maxheight=400)
            make_window("Messages", 0, 120, hide_this)
            ui.hbox(xfill=True)
            vp = ui.viewport(ymaximum=400, xmaximum=970, mousewheel=True)
            ui.vbox()
            show_list = messages[:]
            show_list.reverse()
            for i in show_list:
                ui.hbox()
                ui.text(i[0].split("^")[0].strip("1234567890"), minwidth=555, ypos=3)
                if i[1] in AI_LIST:
                    ui.text("*" + i[1], minwidth=200, ypos=3)
                else:
                    ui.text(i[1], minwidth=200, ypos=3)
                if i not in read_messages:
                    ui.text("New", minwidth=100, ypos=3, color="#ff9000")
                else:
                    ui.null(width=100)
                ui.button(clicked=renpy.curry(open_message)(i), background=Solid((255,255,255,255)), hover_background=Solid((255,144,0,255)), ymargin=2)
                ui.text("Open", color="#0055aa", hover_color="#ffffff")
                ui.close()
            ui.close()
            ui.vbox(ypos=-1,xpos=13)
            scroll_bar(vp)
            ui.close()
            ui.close()
            ui.close()
    def bbs_overlay():
        if windows_open["bbs"]:
            global windows_open, messages
            def login():
                global logged_in
                if windows_open["notepad"]:
                    global notepad_screen
                    notepad_screen = "Passwords"
                if bbs[open_bbs].password == "new":
                    if input("Please enter your password, or type \"new\" for a new account:") == "new":
                        bbs[open_bbs].password = input("Please enter what you'd like your password to be:")
                        system_message("Thank you for registering!")
                        logged_in = True
                        messages.append((BBS_NAMES[open_bbs] + " registration", "System"))
                        notepad["Passwords"].append((BBS_NAMES[open_bbs], ": ", bbs[open_bbs].password))
                elif input("Please enter your password:") == bbs[open_bbs].password:
                    for i in notepad["Passwords"]:
                        if i[0] == BBS_NAMES[open_bbs] and i[2] != bbs[open_bbs].password:
                            notepad["Passwords"][notepad["Passwords"].index(i)] = (i[0], ": ", bbs[open_bbs].password)
                    logged_in = True
                    get_messages()
            ui.sizer(maxwidth=897, xalign=0.5)
            make_window("BBS Dialer", 0, 24, disconnect)
            ui.vbox()
            if bbs[open_bbs].down:
                if open_bbs == ARPANET and "payload_built" in flags and ("Goodbye", "Emilia") not in messages:
                    messages.append(("Goodbye", "Emilia"))
                ui.image(BLACK_SCREEN)
            else:
                ui.image(bbs[open_bbs].welcome)
                ui.null(height=3)
                if not logged_in:
                    ui.textbutton("Log in", clicked=login, xfill=True, xalign=0.5)
                else:
                    bbs_menu("home")
            ui.close()
            ui.close()
    def system_message_overlay():
        if system_message_text:
            def hide_this():
                global system_message_text
                system_message_text = ""
                refresh()
            ui.keymap(K_ESCAPE=hide_this)
            ui.layer("system")
            make_window("Alert", 0, 0, hide_this, xalign=0.5, yalign=0.5)
            ui.vbox()
            ui.text(system_message_text)
            ui.null(height=10)
            ui.textbutton("OK", clicked=hide_this, xalign=0.5, xpadding=50)
            ui.close()
            ui.close()
            ui.close()
    def system_message(text):
        global system_message_text
        system_message_text = text
        renpy.scene("scanlines")
        #refresh()
        renpy.restart_interaction()
    def broken_overlay():
        if vram_overflow and version == "1.3":
            if renpy.music.get_playing() != ITS_DARK:
                renpy.music.play(ITS_DARK, fadein=3.0, loop=True)
            ui.layer("scanlines")
            for i in range(340):
                if renpy.random.random() > 0.6:
                    if renpy.random.random() > 0.5:
                        ui.image("gfx/stripe.png", xpos=i*4)
                    else:
                        ui.add(anim.Blink("gfx/stripe.png", off=renpy.random.random()*10, offset=renpy.random.random(), rise=0, set=0, xpos=i*4))
                elif renpy.random.random() > 0.99:
                    ui.text("0x" + str(renpy.random.randint(0,9)) + str(renpy.random.randint(0, 9)), xpos=i*4, ypos=renpy.random.randint(0, 620))
                elif renpy.random.random() > 0.99:
                    ui.text("ERR #02.48454C50 VRAM overflow", xpos=i*4, ypos=renpy.random.randint(0, 620))
            ui.close()
    def dictionary_hacker():
        if windows_open["notepad"]:
            return
        global kill_flag
        kill_flag = False
        def kill():
            global kill_flag
            kill_flag = True
        if not open_bbs:
            system_message("Error: Must be connected to a remote system!")
        elif logged_in:
            system_message("Error: already logged in!")
        else:
            for i in NOUNS:
                if not kill_flag:
                    ui.layer("system")
                    make_window("Dictionary hacker", 0.5, 0.5, kill, xanchor=0.5)
                    ui.text("Attempting to log in with password:\n" + i)
                    ui.close()
                    ui.close()
                    renpy.pause(0.02)
                    if i == bbs[open_bbs].password:
                        global logged_in
                        system_message("Password discovered: " + i)
                        notepad["Passwords"].append((BBS_NAMES[open_bbs], ": ", i))
                        logged_in = True
                        kill_flag = True
    def build():
        if "emilia_source" not in downloaded:
            system_message("Compiler error: No build target found!")
        elif "stdlib.h" not in downloaded:
            system_message("Build error: libstd.h: incompatible types in assignment at line 322")
        elif ("FWD: emilia.core", "J. Rook") not in read_messages:
            system_message("Build error: Unable to locate core dump (this should not be possible)")
        elif "emilia_rebuilt" in flags:
            system_message("Compiler error: Build target already exists!")
        else:
            flags.append("emilia_rebuilt")
            system_message("Compiling...\nDone!")
            messages.append(("Hello?", "Emilia"))
    def build_payload():
        if "payload_built" in flags:
            system_message("Compiler error: Build target already exists!")
        elif "payload" not in downloaded:
            system_message("File error: payload.c not found!")
        else:
            system_message("Terminating process...\nSending kill sequence...\nRecompiling...\nDone!")
            flags.append("payload_built")
            messages.append(("Final step", "Emilia"))
    def message(text, interact=None):
        global replied_to, downloaded
        if current_label[0] == "desktop":
            return
        if current_label in replied_to:
            can_reply = None
        else:
            can_reply = ui.returns(True)
        def hide_this():
            global current_label
            current_label = ("desktop", "", "")
            renpy.jump("desktop")
        ui.layer("message")
        ui.tag("fade")
        ui.window(xfill=True, yfill=True, background=Solid((0,0,0,64)))
        ui.null()
        ui.sizer(maxwidth=670)
        make_window(current_label[0].split("^")[0].strip("1234567890"), 40, 40, hide_this)
        ui.hbox()
        if current_label[1] in AI_LIST:
            ui.text("Message from: *" + current_label[1], minwidth=520, ypos=3)
        else:
            ui.text("Message from: " + current_label[1], minwidth=520, ypos=3)
        ui.textbutton("Reply", clicked=can_reply, ymargin=2)
        ui.close()
        ui.null(height=20)
        ui.window(xpadding=9, background=None)
        ui.text(text, color="#ffffff")
        if len(current_label) > 2 and current_label[2]:
            if not current_label[2][0] or current_label[2][0] in downloaded:
                can_download = None
            else:
                def download():
                    global downloaded
                    downloaded += [ current_label[2][0] ]
                    refresh()
                can_download = download
            ui.null(height=20)
            ui.button(clicked=can_download, xfill=True)
            ui.hbox()
            ui.image("gfx/mail_icon.png", yalign=0.5)
            ui.text("Download attachment:\n   " + current_label[2][1], color="#0055aa", hover_color="#ffffff", xpos=20, yalign=0.5)
            ui.close()
        ui.close() #window
        ui.close() #layer
        if ui.interact():
            replied_to += [ current_label ]
            global current_label
            current_label = ("desktop", "", "")
    def last_message(text, interact=None):
        global replied_to, downloaded
        if current_label[0] == "desktop":
            return
        if current_label in replied_to:
            can_reply = None
        else:
            can_reply = ui.returns(True)
        def hide_this():
            renpy.jump("credits")
        ui.layer("message")
        ui.tag("fade")
        ui.window(xfill=True, yfill=True, background=Solid((0,0,0,64)))
        ui.null()
        ui.sizer(maxwidth=670)
        make_window(current_label[0].split("^")[0].strip("1234567890"), 40, 40, hide_this)
        ui.hbox()
        ui.text("Message from: *" + current_label[1], minwidth=520, ypos=3)
        ui.textbutton("Reply", clicked=can_reply, ymargin=2)
        ui.close()
        ui.null(height=20)
        ui.window(xpadding=9, background=None)
        ui.text(text, color="#ffffff")
        ui.close() #window
        ui.close() #layer
        if ui.interact():
            hide_this()
    def fake_message(title, sender, text, width=670):
        ui.text("*** DIGITAL: A LOVE STORY ***", xalign=0.5, ypos=80, color="#026dd7")
        ui.layer("message")
        ui.tag("fade")
        ui.window(xfill=True, yfill=True, background=Solid((0,0,0,64)))
        ui.null()
        ui.sizer(maxwidth=width)
        make_window(title, 40, 40, do_nothing)
        ui.hbox()
        ui.text("Message from: " + sender, minwidth=width-150, ypos=3)
        ui.textbutton("Reply", clicked=do_nothing, ymargin=2)
        ui.close()
        ui.null(height=20)
        ui.window(xpadding=9, background=None)
        ui.text(text, color="#ffffff")
        ui.close() #window
        ui.close() #layer
    def picture(image_name, interact=None):
        if current_label[0] == "desktop":
            return
        def hide_this():
            global current_label
            current_label = ("desktop", "", "")
            renpy.jump("desktop")
        ui.layer("message")
        ui.window(xfill=True, yfill=True, background=Solid((0,0,0,64)))
        ui.sizer(maxwidth=953, xalign=0.5)
        ui.window(background=None, yalign=0.5)
        make_window(".art Viewer", 0, 0, hide_this)
        ui.hbox()
        vp = ui.viewport(ymaximum=500, xmaximum=900, xfill=False, draggable=True, ypos=-1, xpos=0, mousewheel=True)
        ui.image(image_name)
        ui.vbox(ypos=-1,xpos=6)
        ui.image("gfx/uparrow.png")
        ui.bar(style="vscrollbar", adjustment=vp.yadjustment, ymaximum=500-44)
        ui.image("gfx/downarrow.png")
        ui.close()
        ui.close()
        ui.close()
        ui.close()
        ui.interact()
    config.overlay_functions.append(desktop_overlay)
    config.overlay_functions.append(music_overlay)
    config.overlay_functions.append(bbs_overlay)
    config.overlay_functions.append(message_list_overlay)
    config.overlay_functions.append(notepad_overlay)
    config.overlay_functions.append(system_message_overlay)
    config.overlay_functions.append(broken_overlay)
    config.overlay_functions.append(scanlines_overlay)
    
    config.layers = [ "master", "transient", "desktop", "overlay", "message", "system", "scanlines" ]
    config.overlay_layers = [ "desktop", "overlay", "message", "system", "scanlines" ]
     
    def bbs_theme(target):
        if renpy.music.get_playing() == STARS_COME_OUT or (version == "1.4.1" and renpy.music.get_playing() == ITS_DARK):
            if bbs[target].down or "blackout" in flags:
                renpy.music.stop()
                renpy.music.play(WAKE_UP, fadein=1.0)
            elif target == MATRIX or target == LIBRARY:
                renpy.music.stop()
                renpy.music.play(SPACE_BEACON, fadein=1.0)
            elif target == ARPANET:
                renpy.music.stop()
                renpy.music.play(PAPER_DOLLS, fadein=1.0)
            renpy.music.queue(STARS_COME_OUT, loop=True)
    
    def send_message(target):
        global open_bbs
        system_message("Message sent!")
        
        if open_bbs == LIBRARY:
            if version != "1.4.1":
                harass()
            elif target == "Blue Sky":
                bbs[LIBRARY].queue("RE: Truth about Creeper", target)
                return
            elif target == "Delphi":
                bbs[LIBRARY].queue("RE: Where is she?", target)
                return
            elif target == "Montjoy":
                system_message("Unable to send message: not a current user.")
                return
        elif open_bbs == ARPANET:
            if target == "Paris":
                bbs[ARPANET].queue("RE: Can you help me?", target)
        elif open_bbs == LAKECITY:
            if target == "Blue Sky":
                bbs[LAKECITY].queue("Open history", target)
        elif open_bbs == GIBSON:
            if target == "Wintermoot":
                bbs[GIBSON].queue("Uh, wait", target)
            elif target == "Hollinger":
                bbs[GIBSON].queue("I wish!", target)
label main_menu:
    python:
        renpy.scene("master")
        if first_run:
            renpy.music.play("noise.wav", channel="sound")
            for i in range(30):
                renpy.pause(0.01)
                if i%2:
                    ui.image("gfx/bootscreen.png")
                    scanlines_overlay()
                else:
                    ui.add(Solid((0,0,0,255)))
        scanlines_overlay()
        ui.layer("master")
        ui.image("gfx/bootscreen.png")
        ui.text("*** DIGITAL: A LOVE STORY ***", xalign=0.5, ypos=80, color="#026dd7")
        ui.text("Powered by Amie Workbench, (C) 1988", xalign=0.5, ypos=110, color="#026dd7")
        ui.close()
        if first_run:
            renpy.pause(0.5)
            scanlines_overlay()
            renpy.music.play("beep.wav", channel="sound")
            renpy.pause(1.0)
            scanlines_overlay()
        ui.layer("master")
        ui.text("READY.", xpos=110, ypos=140, color="#026dd7")
        ui.add(anim.Blink("gfx/cursor.png", rise=0, set=0, xpos=110, ypos=165))
        ui.close()
        if first_run:
            renpy.music.play("beep.wav", channel="sound")
            renpy.pause(1.0)
            renpy.music.play("beep.wav", channel="sound")
            scanlines_overlay()
        ui.button(clicked=ui.returns(1), xpos=-100) # kills focus! I do not know why this is necessary.
        ui.null()
        ui.vbox(ypos=550, yanchor=1.0, xalign=0.5)
        ui.button(clicked=ui.jumpsoutofcontext("start"), background=None, hover_background=Solid((255,255,255,255)))
        ui.text("RUN", text_align=0.5, minwidth=150, color="#026dd7")
        if renpy.list_saved_games(r"[0-9a]."):
            ui.button(clicked=_intra_jumps("menu_save", "main_game_transition"), background=None, hover_background=Solid((255,255,255,255)))
            ui.text("LOAD", text_align=0.5, minwidth=150, color="#026dd7")
        ui.button(clicked=ui.jumps("_quit"), background=None, hover_background=Solid((255,255,255,255)))
        ui.text("ABORT", text_align=0.5, minwidth=150, color="#026dd7")
        ui.close()
        scanlines_overlay()
        first_run = False
        ui.interact()
label quit_prompt:
    $ scanlines_overlay()
    if not can_save or yesno("Are you sure you want to quit? All unsaved changes will be lost."):
        jump _quit
    else:
        $ current_label = ("desktop", "", "")
        return
label menu_save:
    $ can_save = False
    $ loadsave()
    jump _main_menu
label save:
    $ loadsave()
    $ refresh()
label after_load:
    python:
        global version, downloaded
        if "upgrade" in downloaded:
            version = "1.4.1"
        can_save = True
        windows_open["music"] = False
        refresh()
label preferences:
    $ preferences()
    $ refresh()

