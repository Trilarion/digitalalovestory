label illegal_access:
    $ bbs[MATRIX].store("Lake City Archives", "The Finn")
    $ bbs[MATRIX].store("Dead AIs", "Nauseahare")
    $ bbs[MATRIX].store("RE: stdlib.h error", "Rocky", ("stdlib.h", "stdlib.h  7kbytes"))
    if "lcl_archives" not in flags:
        $ flags.append("lcl_archives")
    
    message "Holy crap, guys... does anyone know who this %(screen_name)s person is? I heard from Wintermoot that there's a bunch of posts, but he never actually accepted registration or anything. I'd really like to to give kudos, because again, holy shit, %(screen_name)s HACKED THE GIBSON!!"
    
    jump desktop
    
label re_senator_nguyen:
    message "Are you damaged? It's a Vietnamese name, and I don't know if you've noticed, but the war ended a decade ago! You are possibly the only person I have ever heard of seriously try to use the phrase \"Chinaman\" in a non-ironic manner, and while I don't know what could possibly be wrong with you, I assure you, there's no way race is going to be an issue for Robert Nguyen taking that senator position.\n\nYou are the worst kind of racist and I'm embarassed to be posting on the same board as you."
    
    jump desktop
    
label corebbs_rng_exploit:
    message "A weird trick: when regenerating passwords, the *CoreBBS RNG (random number generator) seems to be incredibly misimplemented. It generates passwords sequentially: so if the first password in a sequence will be xxxxxx0, then the replacement will be xxxxxx1, and the next replacement xxxxxx2, and so forth. I don't know how this bug managed to make it into production, so be wary, anyone thinking of switching to *CoreBBS."
    
    jump desktop
    
label re_corebbs_rng_exploit:
    message "Are you sure that's true? I'm running *CoreBBS on my Amie, and you scared the piss out of me with that report... but I can't replicate it! I've regenerated a user password a dozen times, and each one has gotten a completely different one each time."
    
    jump desktop
    
label re_corebbs_rng_exploit2:
    message "Maybe it's a platform specific bug?"
    
    jump desktop
    
label re_corebbs_rng_exploit3:
    message "It's possible. I discovered this on a PC AT, which does some very weird things architecture-wise, especially with its insane ridiculous \"protected mode.\" I wouldn't really be shocked if things designed for saner computers didn't end up working right on it.\n\nHas anyone else heard of this sort of problem before?"
    
    jump desktop
    
label the_gathering_storm:
    message "## Okay guys, reposting because\n## I know the old one vanished.\n\nHer Cold Blade Part I:\nThe Gathering Storm\n\nEpic fantasy at its finest. The threat of orcish invasion... a mighty warrior queen... a magical artifact... unlikely allies....\n\nWith a battle system inspired by fast-paced console RPGs, a fully customizable character creation system, monsters to slice your way through, and one super sexy lead character, this is one amazing start to the series!"
    
    jump desktop
    
label kingdom_at_war:
    message "Her Cold Blade Part II:\nKingdom at War\n\nNow, the tensions from The Gathering Storm have turned into all out war! And the battle system has been completely overhauled to allow for epic clashes of armies! The human alliance stands brave, with dozens of playable generals ready to push back the endless orc horde! And who is this mysterious warrior of Orcish prophecy, known only as \'Saber\'? Is he the trump card that our mighty warrior queen needs?"
    
    jump desktop
    
label heart_of_fire:
    message "Her Cold Blade Part III:\nHeart of Fire\n\nThe third, controversial installment in the Her Cold Blade series. This one drops the epic combat of the last one, and instead follows Major Mira and Saber on a sidestory. It's good if you're a fan of Mira, but everyone probably just cares what's going on with the QUEEN, you know, that the series is named after. Oh... and the whole story is pretty much about how Mira is a badass lesbian. Who WROTE this?"
    
    jump desktop
    
label queen_and_destroyer:
    message "Her Cold Blade 4:\nQueen and Destroyer\n\nThe series goes back to its roots: maneuvering massive armies to kill hundreds of orcish invaders... starring our sexy warrior queen!\n\nBut like Heart of Fire hinted at, she's now losing the war, and now the allies' only hope is to unlock the mystery behind Saber's prophecy! But will tension in the ranks only pull them further apart?"
    
    jump desktop
    
label her_cold_blade_v:
    message "man I just want to know when the next one is supposed to be coming out. supposedly they've figured out a way to make the battles even MORE epic which sounds pretty fucking awesome by my book. plus travelling across continents? finally getting to see some trolls? holy shit this is pretty much going to be the most badass thing ever"
    
    jump desktop
    
label re_her_cold_blade_v:
    message "Yeah, if it ever actually comes out. It was supposed to come out in summer 1987. Now we still don't even have a release date! It's bullshit."
    
    jump desktop
    
label amie_c_compiler_i:
    message "So, you've decided to to program in C on your Amie, but you don't know where to begin? I'll walk you through the basics of getting a simple program written over the next dozen posts! But the first thing you'll need is obvious: a C compiler."
    
    jump desktop
    
label sector_001_registration:
    $ password = bbs[SECTOR001].password
    
    message "Welcome to Sector 001, your FidoNet connection! Your local account name is %(screen_name)s, and your login password is \"%(password)s\".\n\nPlease note that due to the nature of FidoNet, it may take upwards of several days for a message to reach someone on a different node, depending on how frequently they update. We can't do anything about this."
    
    jump desktop
