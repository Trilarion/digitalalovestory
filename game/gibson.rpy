label ill_be_here_less:
    $ bbs[MATRIX].store("Looking for someone", "J. Rook")
    $ bbs[MATRIX].store("RE: Looking for someone", "Orph3o")
    
    message "Hey, guys, I'm afraid to say that I'm going to be posting here a lot less in the upcoming months. A LOT less, actually, probably not at all. I'm sorry, but anyone who was looking forward to more of Neon Empire will have to wait a while.\n\nThe thing is that little Ichigo's due just about any day now, and after she's born... well, I won't have time to write between changing diapers, I'm sure. I'll miss you guys, and I'll let you know when I'm around again!\n\nRegards,\n Eriko Yamazaki"
    
    jump desktop
    
label re_ill_be_here_less1:
    message "Wow, has it been that long already? Congratulations, Eriko!\n\nDon't worry about it, I'm sure we'll see you again... eventually."
    
    jump desktop
    
label re_ill_be_here_less2:
    message "Congratulations! Maybe you can start her on some REAL books soon, what with your being a writer and all... Baby's First Mirrorshades, maybe? ;-)"
    
    jump desktop
    
label re_ill_be_here_less3:
    message "Noooooo, aw man. Now we'll never find out what happens next to Neo-Tokyo."
    
    jump desktop

label he_doesnt_get_it:
    message "Man, come on. I love the fuck out of Cyberpunk, but Gibson himself... I'm just not feeling it. Sterling, sure, that guy doesn't fuck around, he knows what he's talking about. But come on, Neuromancer, what does that have anything to do with REAL computers?\n\nFor fuck's sake, the man wrote Neuromancer on a manual fucking typewriter! All he knows about computers is just all second-hand. He's got the punk all right, but there ain't nothing REALLY cyber about his world."
    
    jump desktop
    
label re_he_doesnt_get_it1:
    message "You're missing the point entirely. SF isn't supposed to be predictive just for the sake of predicting the future. It's not supposed to be a crystal ball. There's an extrapolative aspect, yes, but even the most extrapolative stories also have strong metaphorical qualities, and Gibson is no exception. It's not supposed to be an earnest examination of the future, rather, it's an exemplar of the social anxieties that underlie the Cyberpunk movement. When he talks about technology, he's not really talking about technology... he's talking about human attachments to technology. That certainly has value."
    
    jump desktop
    
label re_he_doesnt_get_it2:
    message "Okay fine it's not realistic but who CARES it's really cool! That's the important part!"
    
    jump desktop
    
label as_if:
    message "If there's one thing I'm sick of, it's this idea that somehow super-hackers will be able to infiltrate government systems and dance around the authorities. Come on. This is not realistic. In reality, the super-hackers get HIRED by the government and their security is damn well impenetrable. I mean, come on.\n\nEven now, at the height of government incompetence, I challenge you to find even an entry point to a government network! You can't, because they know what the hell they're doing. This notion that the people who invented computer networking wouldn't know anything about security is laughable."
    
    jump desktop
    
label re_as_if:
    if "ARPANET: (805) 524-4742" not in notepad["Phone #s"]:
        $ notepad["Phone #s"].append("ARPANET: (805) 524-4742")
    
    message "network's only as strong as its weakest point. you can get into ARPANET via (805) 524-4742"
    
    jump desktop
    
label re_re_as_if:
    message "...touché. How the hell did you find that?"
    
    jump desktop
    
label re_re_re_as_if:
    message "remember: government doesn't mean white house, government doesn't mean pentagon. government means the local FCC office, or the ambulance dispatcher... or your local university. :)\n\nthat ain't changing in the future."
    
    jump desktop
    
label fastest_fidonet_node:
    message "Well, what is it? I don't care if I have to dial long distance, I'm just sick of messages taking two days to arrive!"
    
    jump desktop
    
label re_fastest_fidonet_node:
    if "Sector 001: (614) 622-1701" not in notepad["Phone #s"]:
        $ notepad["Phone #s"].append("Sector 001: (614) 622-1701")
    
    message "It's probably Sector 001 (614-622-1701), which updates twice a day, if you can stand the chintzy theme (ugh, it's like the worst of the '70s, I can't believe anyone ever took that shit SERIOUSLY). It doesn't really matter, though, since MOST nodes update less than once a day at an awkward time, so the end result is that no matter how fast your node updates, it's always gonna take two days for messages to reach the recepient.\n\nIn general, FidoNet is a pretty practically lousy implementation of a neat idea. It's like networking using the post office."
    
    jump desktop
    
label re_fastest_fidonet_node2:
    message "Well, that sucks."
    
    jump desktop
