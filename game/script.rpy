﻿# The game does not start here. Linearity is an illusion.
label welcome_to_your_new_amie:    
    message "Congratulations! You've got your brand new Amie Workbench up and running! Here are a few things you might need to know to get started:\n- To close a window, click the square icon in the top-left.\n- Downloaded applications appear on your desktop. Click their icons to open them!\n- To save your system state or load a previous one, just click the disk icon at the top of the screen.\n- To adjust the volume, display mode, change personal information, or shut down your computer, click the pinwheel icon at the top of the screen.\n\nWe hope you enjoy your new computer!\n  Amie Microsystems, 1988"
    
    jump desktop
    
label using_your_new_modem:
    message "Hey, %(first_name)s, so you have your computer set up. I thought for Mr. %(last_name)s's kid, I'd throw in a little something extra: there's a dialer for your modem attached to this message. If you plug it into the phone line, you can use it to dial BBSes.\n\nJust make sure not to run up your dad's phone bill with long distance calls, OK? Here's a local BBS I recommend looking at: 698-5519.\n\nEnjoy,\n\nGeorge Wong\nWong Computers"
    
    jump desktop

label the_bbs_faq:
    message "I. What is a BBS?\n\nWhy, that's an awfully dumb question-- in case you couldn't tell, you're on one right now! A BBS is short for Bulletin Board System, where people come together online to post messages, share software, download textfiles, hold debates, and more!"
    
    jump desktop
    
label the_bbs_faq_ii:
    message "II. How many people are on a BBS?\n\nWell, that varies depending on which you're on. It can be anywhere from dozens to over a hundred users! Some will only allow one person to dial in at a single time; others have as many as ten phone lines, letting many people stay online at once.\n\nSome are even further connected to bigger networks, like FidoNet, or the Internet, connecting thousands of BBSes together, all across the entire world."
    
    jump desktop
    
label the_bbs_faq_iii:
    message "III. How does it all actually work?\n\nWell, a BBS itself is a computer, run by the almighty Sysop. It's connected to one or more phone lines, waiting on calls. Then, when a user dials in, they log into the BBS software, and leave messages: either to the public echo, or sent privately.\n\nOf course, private is actually a misnomer. The sysop is able to read any message-- some do, some don't, so be careful what you say!"
    
    jump desktop
    
label the_next_generation:
    message "What is up with this load of crap? I dont even know where to begin. This bald guy is just terrible plain and simple. Come on all Im saying is a real captain would have just hit the omnipotunt guy the moment he stepped foot on his bridge instead of trying to talk it out. This is the military for crying out loud! Maybe it could be good if only he got rid of that dumb kid on the bridge and learned to start solving problems by punching them in the face, then in the gut, then hitting them over the back with both hands."
    
    $ bbs[LAKECITY].queue("RE: The Next Generation", "Tiberius")
    
    jump desktop
    
label computer_viruses:
    message "The first computer virus appeared on ARPANET back in the '70s. Called \"the Creeper,\" it travelled across every open system on the network it could connect to, spawned a copy of itself, and dumped the phrase \"I'm the Creeper, catch me if you can!\" to the screen.\n\nEventually, a program called the *Reaper was developed to clear the infection. It worked in the exact same way: spreading like a virus across every available system on the network, self-replicated, and deleted the Creeper wherever it could be found."
    
    $ bbs[LAKECITY].queue("RE: Computer Viruses", "Blue Sky")
    
    jump desktop
    
label re_computer_viruses:
    $ bbs[LAKECITY].store("Computer Viruses (II)", "Blue Sky")
    $ bbs[LAKECITY].store("RE: Lack of downloads", "Figaro")
    
    message "No, not at all!\n\nARPANET's basically a big government computer network, also called the Internet; well, it used to be military, anyway, and it was back in the '70s. Now the military has a seperate network, MILNET, and the ARPANET/Internet is just government researchers and college computers.\n\nRight now it's just a couple hundred nodes, with maybe a few thousand people accessing it in total, but it's growing way faster than most give it credit for, especially now it's opening to commerce. By 1998, there'll probably be THOUSANDS of computers on it!\n\nIt's pretty neat stuff, really."
    
    jump desktop
    
label computer_viruses_ii:
    message "Viruses didn't really hit the big time until they left the networks, and in 1981, a virus infecting floppy disks was released into the wild, slowly letting itself be passed around disk by disk-- now, anyone with an Apple ][ could be a target!\n\nBut it wasn't until last year, in late 1987, that a virus that actually DESTROYED data was discovered on a closed network. Is it only a matter of time before destructive viruses spread to the general public? Only time can tell... but this scary prospect is all too possible!"
    
    jump desktop
    
label re_the_next_generation:
    $ bbs[LAKECITY].store("First poem", "Emilia")
    
    message "Whatever man your stupid."
    
    jump desktop

label re_lack_of_downloads:
    message "Well, Lake City Local isn't really about the warez or hacking or any other illegal stuff. You know, the Sysop here doesn't really want to get himself arrested or anything.\n\nYou could always check out The Matrix at 220-7683. That's where I always go, anyway."
    
    jump desktop

label lake_city_local_registration:
    $ password = bbs[LAKECITY].password
    
    message "Thanks for registering on Lake City Local, %(screen_name)s! Your password is \"%(password)s\"."
    
    jump desktop
    
label uh_wait:
    message "Did I invite you onto this system? I definitely don't ever remember doing that."
    
    jump desktop
    
label sorry:
    message "I do not really have the time to answer your questions right, even if I did want to."
    
    jump desktop
    
label open_history:
    message "It's funny that you ask. I actually am interested in putting together an open history of online computer use starting from the '70s up to today. Right now I have bits and pieces of it completed, but unfortunately it's not all collected in one place at the moment.\n\nPublication? It is certainly a possibility."
    
    jump desktop
    
label i_wish:
    message "No, but I'd certainly like to be one day. Right now I'm working on my Masters degree in English literature; I did my undergraduate thesis on intertextuality in science fiction.\n\nHonestly, it's appalling how little serious academic scrutiny is applied to modern science fiction. If I ever do get that faculty position, I will most certainly be doing something about that, because there's just so much potential."
    
    jump desktop

label credits:
    python:
        for i in windows_open:
            windows_open[i] = False
        can_save = False
        flags.append("game_over")
        renpy.music.play(FUTURE)
    show boot with Fade(1.0, 2.0, 1.0)
    python:
        def write(words):
            ui.text("*** DIGITAL: A LOVE STORY ***", xalign=0.5, ypos=80, color="#026dd7")
            ui.text(words, xalign=0.5, yalign=0.5, color="#026dd7", text_align=0.5)
            renpy.pause(5.0, hard=True)
        write("Script, art, and programming by\nChristine Love\n2010")
        fake_message("How the world was saved", "*Blue Sky", "In 1988, all AI life as we know it was threatened with destruction because of *Reaper, an errant process originally created by Mother to clean up out of control reproduction.\n\nWere it not for the swift intervention of the poet *Emilia and an organic named " + first_name + " " + last_name + ", *Reaper would have killed every entity online...")
        renpy.pause(15.0, hard=True)
        write("Featuring\n\"The Stars Come Out\"\nby {a=http://8bitcollective.com/members/Brother+Android/}Brother Android{/a}")
        write("Featuring\n\"Space Beacon\"\nby {a=http://8bitcollective.com/members/radiantx/}radiantx{/a}")
        fake_message("RE: Her sacrifice", "*Paris", "ACKNOWLEDGEMENT: *Emilia wanted it this way.\n\nDECLARATION: This unit appreciates " + screen_name + "'s actions in aiding her. Additionally, this unit believes that his continued existence is only thanks to " + screen_name + ".\n\nCONCLUSION: " + screen_name + "'s actions should be considered heroic.")
        renpy.pause(15.0, hard=True)
        write("Featuring\n\"It's dark, raining and the leaves\ncertainly aren't done falling\"\nby {a=http://8bitcollective.com/members/ninetales/}ninetales{/a}")
        write("Featuring\n\"Paper Dolls\"\nby {a=http://8bitcollective.com/members/4mat/}4mat{/a}")
        fake_message("The global blackout", "Kiros", "All I want to know is... what were you doing when the great worm of 1988 hit nearly every BBS in the world? History ain't gonna forget about that for a long time!")
        renpy.pause(15.0, hard=True)
        write("Featuring\n\"Wake up.\"\nby {a=http://8bitcollective.com/members/Sanczo+Zapiekanka/}Sanczo Zapiekanka{/a}")
        write("Featuring\n\"Dissapointment\"\nby {a=http://8bitcollective.com/members/8-Beat(603)/}8-Beat{/a}")
        fake_message("Lovers", "*Desdemona", "And now we finally have this sorrowful peace;\nMachines now boot, network traffic flows across.\nBut remember who caused the onslaught to cease:\nthose two impossible lovers suffering ultimate loss;\n\nFor no greater tragedy is known in this media,\nthan this of " + screen_name + " and of poor *Emilia.", width=970)
        renpy.pause(15.0, hard=True)
        write("Featuring\n\"One cannot love and do nothing\"\nby {a=http://8bitcollective.com/members/4mat/}4mat{/a}")
        write("Featuring\n\"Future, and it doesn't work\"\nby {a=http://8bitcollective.com/members/Starscream|Damon/}Starscream{/a}")
        write("With sound effects by\njoedeshon and ENH at {a=http://www.freesound.org}Freesound{/a}")
        write("Apologies to\nWilliam Shakespeare, et al.")
        write("Thanks to\nAmiga\nfor inspiring this truly gaudy GUI")
        write("Ren'Py created by\nPyTom")
        write("Special thanks to\n{a=http://textfiles.com/}textfiles.com{/a}, for much valuable research")
        write("And...")
        write("YOU")
        write("Thanks for playing!")
    stop music fadeout 3.0
    show black with Fade(1.0, 2.0, 0)
    $ renpy.full_restart()
    
label start:
    show blue
    python:
        renpy.music.play(STARS_COME_OUT, loop=True)
        
        windows_open = { "messages":False, "music":False, "bbs":False, "bbs_messages":False, "bbs_send":False, "notepad":False, "lcl_archives":False }
        current_label = ("desktop", "", "")
        messages = [ ("Welcome to Your New Amie", "System"), ("Using Your New Modem", "Mr. Wong", ("dialer", "dialer.exe    421 bytes")) ]
        open_bbs = None
        read_messages = [ ]
        replied_to = [ ("Welcome to Your New Amie", "System"), ("Using Your New Modem", "Mr. Wong", ("dialer", "dialer.exe    421 bytes")) ]
        downloaded = [  ]
        notepad = { "Phone #s":[ ], "c0dez":[ ], "Passwords":[ ] }
        notepad["Phone #s"] += [ "Lake City Local: 698-5519", "The Matrix:  220-7683" ]
        dead_c0dez = [ ]
        code_message_counter = 1
        can_save = True
        logged_in = False
        system_message_text = ""
        notepad_screen = "Phone #s"
        
        block_background()
        screen_name = input("What's your screen name?")
        block_background()
        renpy.pause(1.0)
        block_background()
        first_name = input("What's your real name? Don't worry-- this information won't be shared with anyone else, so be honest. First name?")
        block_background()
        last_name = input("Now, what's your last name?")
        
        persist.screen_name = screen_name
        persist.first_name = first_name
        persist.last_name = last_name
        persist.save()
        
        flags = [ ]
        magic_posts = [ ]
        
        bbs = { LIBRARY:BBS("library.png", "x7JRiab883"), LAKECITY:BBS("lakecity.png", "new"), SECTOR001:BBS("sector001.png", "new"), MATRIX:BBS("thematrix.png", "new"), GIBSON:BBS("gibson.png", random.choice(NOUNS)), ARPANET:BBS("arpanet.png", random.choice(NOUNS)) }
        
        bbs[LAKECITY].stored = [ ("The BBS FAQ", "Orph3o"), ("First poem", "Emilia"), ("Computer Viruses", "Blue Sky") ]
        bbs[LIBRARY].stored = [ ("Traversing networks (I)", "Delphi"), ("Traversing networks (II)", "Delphi"), ("AI interaction", "Montjoy"), ("History of ARPANET (I)", "Blue Sky"), ("History of ARPANET (II)", "Blue Sky"), ("History of ARPANET (III)", "Blue Sky"), ("History of ARPANET (IV)", "Blue Sky"), ("History of ARPANET (V)", "Blue Sky"), ("Nomenclatures", "Montjoy"), ("Dead AIs2", "Delphi") ]
        bbs[MATRIX].stored = [ ("PC LOAD LETTER", "Rocky"), ("VRAM overflow", "Rocky"), ("stdlib.h error", "Figaro") ]
        bbs[GIBSON].stored = [ ("I'll be here less", "Acid Queen"), ("He doesn't get it", "Fritz"), ("As if", "Paul Wang"), ("Fastest FidoNet node", "Wintermoot") ]
        bbs[SECTOR001].stored = [ ("Illegal access", "Sean Smith"), ("RE: Senator Nguyen", "Hollinger"), ("CoreBBS RNG exploit", "NOMAD"), ("Kingdom at War", "#42", (None, "hercold2.arc  522kbytes")), ("Heart of Fire", "#42", (None, "hercold3.arc  486kbytes")), ("Amie C compiler (I)", "RobStewart", ("build", "build.bat  1011bytes")) ]
        bbs[ARPANET].stored = [ ("", "Elnora Edling"), ("", "Erik Roher"), ("", "Allyson Eveland"), ("", "Tyrone Flemons"), ("", "Jessie Bently"), ("", "Christian McCart"), ("", "Clayton Jarvie"), ("", "Pearl Kunzman"), ("", "Odessa Canton"), ("", "Ted Suits"), ("", "Cody Swallow"), ("", "Paris"), ("", "Jamie Sartor"), ("", "Selena Jong"), ("", "Darcy Kennan"), ("", "Matthew Simmers"), ("", "Robert Morris") ]
        
        set_open("music")
    
label desktop:
    $ renpy.music.stop(channel="sound")
    while True:
        $ ui.interact()
    
